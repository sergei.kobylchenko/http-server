const myHttp = require('./http');
const fs = require('mz/fs');

const server = myHttp.createServer();

server.on('request', (req, res) => {
  console.log(req.headers, req.method, req.url);
  
  res.setHeader('Content-Type', 'application/json');
  res.writeHead(200) //Вызов writeHead опционален
  fs.createReadStream('somefile.txt').pipe(res);
  
});